.. Sun/OS Linux Documentation documentation master file, created by
   sphinx-quickstart on Tue May  4 20:33:30 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sun/OS Linux documentation!
======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
